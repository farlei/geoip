//
//  ViewController.swift
//  GeoIP
//
//  Created by Farlei Heinen on 12/05/15.
//  Copyright (c) 2015 Paperkite. All rights reserved.
//

import UIKit
import Snap
import Datakit

class ViewController: UIViewController {

    static let regularFontSize = CGFloat(25)
    
    let extraInfoKey = "extraInfoKey"
    let appGroupKey = "group.com.farlei.geoip"
    
    var backgroundView : UIView = {
        var backView = UIView()
        backView.backgroundColor = UIColor.whiteColor()
        return backView
    }()
    
    var refreshButton : UIButton = {
        var _refreshButton = UIButton.buttonWithType(.Custom) as! UIButton
        _refreshButton.setTitle("refresh", forState: .Normal)
        _refreshButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        _refreshButton.setTitleColor(UIColor.darkGrayColor(), forState: .Highlighted)
        _refreshButton.setTitleColor(UIColor.lightGrayColor(), forState: .Disabled)
        _refreshButton.backgroundColor = UIColor.darkGrayColor()
        _refreshButton.layer.cornerRadius = 8.0
        
        return _refreshButton
    }()
    
    var ipLabel : UILabel = {
        var _ipLabel = UILabel()
        _ipLabel.text = " - "
        _ipLabel.textAlignment = .Center
        _ipLabel.font = UIFont.boldSystemFontOfSize(35)
        _ipLabel.textColor = UIColor.blackColor()
        return _ipLabel
    }()
    
    var ispLabel : UILabel = {
        var _ispLabel = UILabel()
        _ispLabel.text = " - "
        _ispLabel.textAlignment = .Center
        _ispLabel.font = UIFont.systemFontOfSize(ViewController.regularFontSize)
        _ispLabel.textColor = UIColor.blackColor()
        _ispLabel.userInteractionEnabled = true
        return _ispLabel
    }()
    
    var cityLabel : UILabel = {
        var _cityLabel = UILabel()
        _cityLabel.text = " - "
        _cityLabel.textAlignment = .Center
        _cityLabel.font = UIFont.systemFontOfSize(ViewController.regularFontSize)
        _cityLabel.textColor = UIColor.blackColor()
        _cityLabel.userInteractionEnabled = true
        return _cityLabel
    }()
    
    var countryLabel : UILabel = {
        var _countryLabel = UILabel()
        _countryLabel.text = " - "
        _countryLabel.textAlignment = .Center
        _countryLabel.font = UIFont.systemFontOfSize(ViewController.regularFontSize)
        _countryLabel.textColor = UIColor.blackColor()
        _countryLabel.userInteractionEnabled = true
        return _countryLabel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.backgroundView)
        self.refreshButton.addTarget(self, action: "refresh", forControlEvents: .TouchUpInside)
        self.backgroundView.addSubview(self.refreshButton)
        self.backgroundView.addSubview(self.ipLabel)
        
        self.backgroundView.addSubview(self.ispLabel)
        self.backgroundView.addSubview(self.cityLabel)
        self.backgroundView.addSubview(self.countryLabel)
        
        setActiveInfo(self.cityLabel)
        
        let tapISP = UITapGestureRecognizer(target: self, action: "handleTap:")
        let tapCity = UITapGestureRecognizer(target: self, action: "handleTap:")
        let tapCountry = UITapGestureRecognizer(target: self, action: "handleTap:")
        self.ispLabel.addGestureRecognizer(tapISP)
        self.cityLabel.addGestureRecognizer(tapCity)
        self.countryLabel.addGestureRecognizer(tapCountry)
        
        setupAutolayout()
        
        refresh()
    }
    
    func handleTap(gesture: UITapGestureRecognizer) {
        setActiveInfo(gesture.view as! UILabel)
    }
    
    func setActiveInfo(label: UILabel) {
        
        let groupDefaults = NSUserDefaults(suiteName: appGroupKey)
        
        switch label {
        case self.ispLabel :
            groupDefaults?.setObject("isp", forKey: extraInfoKey)
            self.ispLabel.font = UIFont.boldSystemFontOfSize(ViewController.regularFontSize)
            self.countryLabel.font = UIFont.systemFontOfSize(ViewController.regularFontSize)
            self.cityLabel.font = UIFont.systemFontOfSize(ViewController.regularFontSize)
        case self.countryLabel :
            groupDefaults?.setObject("country", forKey: extraInfoKey)
            self.countryLabel.font = UIFont.boldSystemFontOfSize(ViewController.regularFontSize)
            self.ispLabel.font = UIFont.systemFontOfSize(ViewController.regularFontSize)
            self.cityLabel.font = UIFont.systemFontOfSize(ViewController.regularFontSize)
        default : // city
            groupDefaults?.setObject("city", forKey: extraInfoKey)
            self.cityLabel.font = UIFont.boldSystemFontOfSize(ViewController.regularFontSize)
            self.countryLabel.font = UIFont.systemFontOfSize(ViewController.regularFontSize)
            self.ispLabel.font = UIFont.systemFontOfSize(ViewController.regularFontSize)
        }
        
        groupDefaults?.synchronize()
    }
    
    func setupAutolayout() {
        self.backgroundView.snp_makeConstraints { make in
            make.top.equalTo(self.view).offset(25)
            make.bottom.equalTo(self.view).offset(-5)
            make.left.equalTo(self.view).offset(5)
            make.right.equalTo(self.view).offset(-5)
        }
        
        self.refreshButton.snp_makeConstraints { make in
            make.top.equalTo(self.backgroundView).offset(20)
            make.centerX.equalTo(self.backgroundView)
            make.width.equalTo(150)
            make.height.equalTo(30)
        }
        
        self.ipLabel.snp_makeConstraints { make in
            make.top.equalTo(self.refreshButton.snp_bottom).offset(40)
            make.centerX.equalTo(self.backgroundView)
            make.left.equalTo(self.backgroundView)
            make.right.equalTo(self.backgroundView)
        }
        
        self.ispLabel.snp_makeConstraints { make in
            make.top.equalTo(self.ipLabel.snp_bottom).offset(5)
            make.centerX.equalTo(self.backgroundView)
            make.left.equalTo(self.backgroundView)
            make.right.equalTo(self.backgroundView)
        }
        
        self.cityLabel.snp_makeConstraints { make in
            make.top.equalTo(self.ispLabel.snp_bottom).offset(5)
            make.centerX.equalTo(self.backgroundView)
            make.left.equalTo(self.backgroundView)
            make.right.equalTo(self.backgroundView)
        }
        
        self.countryLabel.snp_makeConstraints { make in
            make.top.equalTo(self.cityLabel.snp_bottom).offset(5)
            make.centerX.equalTo(self.backgroundView)
            make.left.equalTo(self.backgroundView)
            make.right.equalTo(self.backgroundView)
        }
    }
    
    func refresh() {
        self.refreshButton.enabled = false
        DataManager.sharedInstance.fetchGeoIP { (geoip, error) -> Void in
            if error == nil {
                if let geoip = geoip {
                    self.ipLabel.text = geoip.ip ?? " (na) " // Nil Coalescing Operator
                    self.ispLabel.text = geoip.isp ?? " (na) "
                    self.cityLabel.text = geoip.city ?? " (na) "
                    self.countryLabel.text = geoip.country ?? " (na) "
                }
            }
            self.refreshButton.enabled = true
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return false
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
}

