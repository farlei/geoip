//
//  AppDelegate.swift
//  GeoIP
//
//  Created by Farlei Heinen on 12/05/15.
//  Copyright (c) 2015 Paperkite. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        self.window = UIWindow()
        if let win = self.window {
            win.frame = UIScreen.mainScreen().bounds
            win.rootViewController = ViewController()
            win.makeKeyAndVisible()
        }
                
        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        return true
    }

}

