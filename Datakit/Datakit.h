//
//  Datakit.h
//  Datakit
//
//  Created by Farlei Heinen on 12/05/15.
//  Copyright (c) 2015 Paperkite. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Datakit.
FOUNDATION_EXPORT double DatakitVersionNumber;

//! Project version string for Datakit.
FOUNDATION_EXPORT const unsigned char DatakitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Datakit/PublicHeader.h>


