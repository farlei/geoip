//
//  GeoIP.swift
//  GeoIP
//
//  Created by Farlei Heinen on 13/05/15.
//  Copyright (c) 2015 Paperkite. All rights reserved.
//

import Foundation
import JSONJoy

public struct GeoIP : JSONJoy {
    public var latitude: Double?
    public var longitude: Double?
    public var city: String?
    public var country: String?
    public var ip: String?
    public var isp: String?
    
    public init() { }
    
    public init(_ decoder: JSONDecoder) {
        latitude = decoder["latitude"].double
        longitude = decoder["longitude"].double
        city = decoder["city"].string
        country = decoder["country"].string
        ip = decoder["ip"].string
        isp = decoder["isp"].string
    }
}