//
//  DataManager.swift
//  GeoIP
//
//  Created by Farlei Heinen on 13/05/15.
//  Copyright (c) 2015 Paperkite. All rights reserved.
//

import Foundation
import Alamofire
import JSONJoy

public class DataManager {
    
    public static let sharedInstance = DataManager() // Singleton
    
    public func fetchGeoIP(done: (GeoIP?, NSError?) -> Void) {
        
        Alamofire.request(.GET, "http://www.telize.com/geoip")
            .responseJSON { (_, _, JSON, error) in
                
                println(JSON)
              
                if let JSON = JSON as? NSDictionary {
                    let geoip = GeoIP(JSONDecoder(JSON))
                    done(geoip,error)
                } else {
                    done(nil,error)
                }
            }
    }
}