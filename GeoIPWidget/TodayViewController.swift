//
//  TodayViewController.swift
//  GeoIPWidget
//
//  Created by Farlei Heinen on 12/05/15.
//  Copyright (c) 2015 Paperkite. All rights reserved.
//

import UIKit
import NotificationCenter
import Datakit

class TodayViewController: UIViewController, NCWidgetProviding {
    
    let extraInfoKey = "extraInfoKey"
    let appGroupKey = "group.com.farlei.geoip"
    
    @IBOutlet weak var ipLabel: UILabel!
    @IBOutlet weak var extraLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.preferredContentSize = CGSize(width: 0.0, height: 150.0)
        
        self.moreButton.layer.cornerRadius = 8.0
        
        refresh()
    }
    
    func getExtraInfo() -> String {
        
        let groupDefaults = NSUserDefaults(suiteName: appGroupKey)
        if let key = groupDefaults?.objectForKey(extraInfoKey) as? String {
            return key
        }
        return "isp"
    }
    
    @IBAction func moreInfo(sender: UIButton) {

        if let url = NSURL(string: "geoip://") {
            self.extensionContext?.openURL(url, completionHandler: nil)
        }
    }
    
    func refresh() {
        DataManager.sharedInstance.fetchGeoIP { (geoip, error) -> Void in
            if error == nil {
                if let geoip = geoip {
                    self.ipLabel.text = geoip.ip ?? " (na) "
                    
                    switch self.getExtraInfo() {
                    case "city" :
                        self.extraLabel.text = geoip.city ?? " (na) "
                    case "country" :
                        self.extraLabel.text = geoip.country ?? " (na) "
                    default:
                        self.extraLabel.text = geoip.isp ?? " (na) "
                    }
                    
                }
            }
        }
    }
    
    func widgetMarginInsetsForProposedMarginInsets(defaultMarginInsets: UIEdgeInsets) -> UIEdgeInsets {
        
        return UIEdgeInsetsZero
    }
    
//    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)!) {
//        completionHandler(NCUpdateResult.NewData)
//    }
    
}
